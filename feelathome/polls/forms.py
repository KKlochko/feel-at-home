#################################################################################
# Copyright (C) 2022 Kostiantyn Klochko <kostya_klochko@ukr.net>                #
#                                                                               #
# This file is part of feel-at-home.                                            #
#                                                                               #
# feel-at-home is free software: you can redistribute it and/or modify it under #
# the terms of the GNU Affero General Public License as published by the Free   #
# Software Foundation, either version 3 of the License, or (at your option)     #
# any later version.                                                            #
#                                                                               #
# feel-at-home is distributed in the hope that it will be useful, but WITHOUT   #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS #
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more  #
# details.                                                                      #
#                                                                               #
# You should have received a copy of the GNU Affero General Public License      #
# along with feel-at-home. If not, see <https://www.gnu.org/licenses/>.         #
#################################################################################
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator
from django.core.exceptions import ValidationError
from django.urls import reverse_lazy
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

class GoodBuyForm(forms.Form):
    def __init__(self, *args, **kwargs):
        validators = []
        min_value = kwargs.pop("min_value", None)
        max_value = kwargs.pop("max_value", None)
        count = kwargs.pop("count", None)
        form_type = kwargs.pop("form_type", None)
        if not min_value is None:
            validators.append(MinValueValidator(min_value))
        if not max_value is None:
            validators.append(MaxValueValidator(max_value))
        if count is None:
            count = 1
        super().__init__(*args, **kwargs)
        self.fields['count'] = forms.IntegerField(
            initial=count,
            validators=validators,
        )
        self.helper = FormHelper()
        # if a good page
        if form_type is None:
            self.helper.add_input(Submit('submit', 'Buy', css_class='btn-success'))
        # if a cart item page
        elif form_type is "change":
            self.helper.add_input(Submit('submit', 'Change', css_class='btn-success'))
            self.helper.add_input(Submit('cancel', 'Cancel', css_class='btn-danger', formnovalidate='formnovalidate'))
        self.helper.form_method = 'POST'

class RegisterForm(UserCreationForm):
    email = forms.EmailField(required=True)
    first_name = forms.CharField(max_length = 32)
    last_name = forms.CharField(max_length = 32)

    class Meta:
        model = User
        fields = ["username", "first_name", "last_name", "email", "password1", "password2"]

    def clean(self):
        email = self.cleaned_data.get('email')
        if User.objects.filter(email=email).exists():
            raise ValidationError("Email exists")
        return self.cleaned_data
#----------------------------- CartConfirmForm ----------------------------
stripeValidator = RegexValidator(r'[0-9]{4} [0-9]{4} [0-9]{4} [0-9]{4}',
                                 'Only digits with separation allowed. Example: dddd dddd dddd dddd.')
mmyy = RegexValidator(r'[0-2][0-9][/][0-9]{2}', 'Use only format mm/yy. Example: 06/23.')
numberValidator = RegexValidator(r'[0-9]{3}', 'Only digits allowed.')

class CartConfirmForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-success'))
        self.helper.add_input(Submit('cancel', 'Cancel', css_class='btn-danger', formnovalidate='formnovalidate'))
        self.helper.form_method = 'POST'

        self.fields['destination'] = forms.CharField(max_length=256)
        self.fields['card'] = forms.CharField(min_length=19, max_length=19, validators=[stripeValidator])
        self.fields['mm_yy'] = forms.CharField(min_length=5, max_length=5, validators=[mmyy])
        self.fields['cvv'] = forms.CharField(min_length=3, max_length=3, validators=[numberValidator])
