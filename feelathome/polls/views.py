#################################################################################
# Copyright (C) 2022 Kostiantyn Klochko <kostya_klochko@ukr.net>                #
#                                                                               #
# This file is part of feel-at-home.                                            #
#                                                                               #
# feel-at-home is free software: you can redistribute it and/or modify it under #
# the terms of the GNU Affero General Public License as published by the Free   #
# Software Foundation, either version 3 of the License, or (at your option)     #
# any later version.                                                            #
#                                                                               #
# feel-at-home is distributed in the hope that it will be useful, but WITHOUT   #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS #
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more  #
# details.                                                                      #
#                                                                               #
# You should have received a copy of the GNU Affero General Public License      #
# along with feel-at-home. If not, see <https://www.gnu.org/licenses/>.         #
#################################################################################
from django.db.models.query import QuerySet
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.core.exceptions import PermissionDenied

from .decorators import forbidden_cart_item, forbidden_order
from .forms import CartConfirmForm, GoodBuyForm, RegisterForm

from .models import Good, Order, Container, ContainersAndOrder
from .filters import GoodFilter, OrderFilter, ContainerFilter

from datetime import datetime
############################### Goods ################################

def index(request):
    goods_list = Good.objects.all()
    goods_filter = GoodFilter(request.GET, queryset=goods_list)
    context = {'goods_filter': goods_filter}
    if request.method == "POST":
        good_id = request.POST.get("good-id")
        if not request.user.is_authenticated:
            return redirect('polls:login')
        add_to_cart(request, good_id)

    return render(request, 'polls/index.html', context)

def detail(request, good_id):
    good = get_object_or_404(Good, pk=good_id)
    buy_form = GoodBuyForm(request.POST or None, min_value=1, max_value=good.count)
    context = {'good': good, 'buy_form': buy_form}
    if request.method == "POST":
        if buy_form.is_valid():
            count = buy_form.cleaned_data.get("count")
            if count != None:
                if not request.user.is_authenticated:
                    return redirect('polls:login')
                add_to_cart(request, good_id, count)
            
    return render(request, 'polls/detail_good.html', context)

############################### Orders ###############################

@login_required(login_url='polls:login')
def orders(request):
    user_id = request.user.id
    user_groups = list(map(lambda g: g.name,request.user.groups.all()))

    if 'admin' in user_groups:
        order_list = Order.objects.all()
    elif 'courier' in user_groups:
        order_list = Order.objects.filter(status__in=["DOING","DONE","CANCELED"]).all()
    elif 'shop assistant' in user_groups:
        order_list = Order.objects.filter(status__in=["TODO","DOING","DONE","CANCELED"]).all()
    else:
        order_list = Order.objects.filter(receiver_id=user_id, status__in=["TODO","DOING","CONFIRM","DONE","CANCELED"]).all()

    orders_filter = OrderFilter(request.GET, queryset=order_list)
    context = {'orders_filter': orders_filter}
    return render(request, 'polls/orders.html', context)

@login_required(login_url='polls:login')
@forbidden_order
def order_detail(request, order_id):
    order = get_object_or_404(Order, pk=order_id)
    user_groups = list(map(lambda g: g.name,request.user.groups.all()))
    containers_ids = ContainersAndOrder.objects.select_related().filter(orderId_id = order_id).values('containerId_id')
    containers = Container.objects.filter(id__in=containers_ids)
    containers_filter = ContainerFilter(request.GET, queryset=containers)
    context = {'order': order, 'containers_filter': containers_filter, 'user_groups': user_groups}
    if request.method == "POST":
        if "TODO" in request.POST:
            print("TODO")
            order.status = "DOING"
        elif "DONE" in request.POST:
            print("DONE")
            order.status = "CONFIRM"
        elif "CONFIRM" in request.POST:
            print("CONFIRM")
            order.status = "DONE"
        order.save()
    return render(request, 'polls/order.html', context)

############################### Cart ###############################
@login_required(login_url='polls:login')
def cart(request):
    user_id = request.user.id
    cart, created = get_cart(request)


    containers_ids = ContainersAndOrder.objects.select_related().filter(orderId_id = cart.id).values('containerId_id')
    containers = Container.objects.filter(id__in=containers_ids)
    containers_filter = ContainerFilter(request.GET, queryset=containers)
    context = {'cart': cart, 'containers_filter': containers_filter}
    if request.method == "POST":
        delete_container_id = request.POST.get("delete-container-id")
        if not delete_container_id is None:
            print(f"{delete_container_id=}")
            container = get_object_or_404(Container, pk=delete_container_id)
            good = get_object_or_404(Good, pk=container.goodId.id)
            good.count += container.count
            good.save()
            obj = Container.objects.filter(id=delete_container_id).delete()
    if is_cart_empty(request):
        context["empty_cart"] = True
    return render(request, 'polls/cart_items.html', context)

@login_required(login_url='polls:login')
@forbidden_cart_item
def cart_item(request, container_id):
    user_id = request.user.id
    cart, created = get_cart(request)

    container = get_object_or_404(Container, pk=container_id)
    good = get_object_or_404(Good, pk=container.goodId.id)
    max_count = good.count + container.count
    context = {'container': container}
    if request.method == "POST":
        if "cancel" in request.POST:
            return redirect('polls:cart')
        container_form = GoodBuyForm(request.POST or None, min_value=1, max_value=max_count, form_type="change")

        if container_form.is_valid():
            count = container_form.cleaned_data["count"]
            if count != None:
                good.count = good.count + container.count - count
                container.count = count
                container.save()
                good.save()
                return redirect('polls:cart')
        else:
            context['container_form'] = container_form
    else:
        container_form = GoodBuyForm(count=container.count, min_value=1, max_value=max_count, form_type="change")
        context['container_form'] = container_form
    return render(request, 'polls/cart_item.html', context)

def add_to_cart(request, good_id, count = 1):
    good = Good.objects.filter(id=good_id).first()
    if good is None:
        return

    cart, created = get_cart(request)

    containers_ids = ContainersAndOrder.objects.select_related().filter(orderId_id = cart).values('containerId_id')
    containers = Container.objects.filter(id__in=containers_ids)

    container, created = containers.get_or_create(
        name = good.name,
        goodId_id = good.id,
        defaults = {
            'discount': 0,
            'price': 0,
            'count': 1,
        },
    )

    if created:
        # Create relation between a container and an order if the container is new.
        containerAndOrder, created = ContainersAndOrder.objects.get_or_create(
            orderId = cart,
            containerId = container,
        )
        # add price to the container
        container.price = good.price
        container.count = count
        # Save model changes in DB.
        container.save()
    else:
        container.count += count
        # Save model changes in DB.
        container.save()
    good.count -= count
    good.save()

def get_cart(request):
    """
    Create the cart if not exists.
    Return cart, created
    """
    user_id = request.user.id
    cart, created = Order.objects.get_or_create(
        receiver_id = user_id,
        status = "CART",
        defaults = {
            'order_date': datetime.now(),
            'price': 0,
            'destination': '',
        },
    )
    return cart, created

def get_cart_price(request):
    """
    Return cart price.
    """
    user_id = request.user.id
    cart, created = get_cart(request)

    containers_ids = ContainersAndOrder.objects.select_related().filter(orderId_id = cart.id).values('containerId_id')
    containers = Container.objects.filter(id__in=containers_ids)
    price = 0
    for container in containers:
        price += container.price * (1 - container.discount)
    return price

def is_cart_empty(request) -> bool:
    """
    Return True if no items in the cart.
    """
    user_id = request.user.id
    cart, created = get_cart(request)

    containers_ids = ContainersAndOrder.objects.select_related().filter(orderId_id = cart.id).values('containerId_id')
    containers = Container.objects.filter(id__in=containers_ids)
    if len(containers) == 0:
        return True
    return False

@login_required(login_url='polls:login')
def cart_confirm(request):
    user_id = request.user.id
    cart, created = get_cart(request)
    if is_cart_empty(request):
        return render(request, 'polls/cart_confirm.html', {})
    cart.price = get_cart_price(request)
    cart.save()

    confirm_form = CartConfirmForm(request.POST or None)
    context = {'cart': cart, 'confirm_form': confirm_form, 'price': f"{cart.price:.2f}"}
    if request.method == "POST":
        if "cancel" in request.POST:
            return redirect('polls:cart')
        if confirm_form.is_valid():
            destination = confirm_form.cleaned_data.get("destination")
            if not destination is None:
                cart.destination = destination
                cart.status = "TODO"
                cart.order_date = datetime.now()
                cart.save()
            return redirect('polls:orders')
    return render(request, 'polls/cart_confirm.html', context)

############################### User/Auth ############################
@login_required(login_url='polls:login')
def cabinet(request):
    return render(request, 'polls/cabinet.html', {})

def sign_up(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            group = Group.objects.get(name="customer")
            user.groups.add(group)
            login(request, user)
            return redirect('/')
    else:
        form = RegisterForm()
    return render(request, 'registration/sign_up.html', {"form": form})

def change_password(request):
    # Doing
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('/')
    else:
        form = RegisterForm()
    return render(request, 'registration/change_password.html', {"form": form})
