#################################################################################
# Copyright (C) 2022 Kostiantyn Klochko <kostya_klochko@ukr.net>                #
#                                                                               #
# This file is part of feel-at-home.                                            #
#                                                                               #
# feel-at-home is free software: you can redistribute it and/or modify it under #
# the terms of the GNU Affero General Public License as published by the Free   #
# Software Foundation, either version 3 of the License, or (at your option)     #
# any later version.                                                            #
#                                                                               #
# feel-at-home is distributed in the hope that it will be useful, but WITHOUT   #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS #
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more  #
# details.                                                                      #
#                                                                               #
# You should have received a copy of the GNU Affero General Public License      #
# along with feel-at-home. If not, see <https://www.gnu.org/licenses/>.         #
#################################################################################
from django.conf.urls import include
from django.urls import path

from . import views

app_name = 'polls'
urlpatterns = [
    path('', views.index, name='index'),
    path('good/<int:good_id>/', views.detail, name='detail'),
    path('', include('django.contrib.auth.urls')),
    path('sign-up', views.sign_up, name='sign_up'),
    path('cabinet', views.cabinet, name='cabinet'),
    path('orders', views.orders, name='orders'),
    path('order/<int:order_id>/', views.order_detail, name='order_detail'),
    path('cart', views.cart, name='cart'),
    path('cart/confirm', views.cart_confirm, name='cart_confirm'),
    path('cart/<int:container_id>/', views.cart_item, name='cart_item'),
]
