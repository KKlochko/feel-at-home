#################################################################################
# Copyright (C) 2022 Kostiantyn Klochko <kostya_klochko@ukr.net>                #
#                                                                               #
# This file is part of feel-at-home.                                            #
#                                                                               #
# feel-at-home is free software: you can redistribute it and/or modify it under #
# the terms of the GNU Affero General Public License as published by the Free   #
# Software Foundation, either version 3 of the License, or (at your option)     #
# any later version.                                                            #
#                                                                               #
# feel-at-home is distributed in the hope that it will be useful, but WITHOUT   #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS #
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more  #
# details.                                                                      #
#                                                                               #
# You should have received a copy of the GNU Affero General Public License      #
# along with feel-at-home. If not, see <https://www.gnu.org/licenses/>.         #
#################################################################################

import django_filters
from .models import Good, Container, Order

class GoodFilter(django_filters.FilterSet):
    o = django_filters.OrderingFilter(
        fields=(
            ('id', 'id'),
            ('name', 'name'),
            ('price', 'price'),
        ),
        field_labels={
            'name': 'Good name',
        }
    )

    class Meta:
        model = Good
        fields = {
            'name': ['icontains'],
            'price': ['gt','lt'],
            'count': ['gt','lt'],
        }

class OrderFilter(django_filters.FilterSet):
    o = django_filters.OrderingFilter(
        fields=(
            ('id', 'id'),
            ('price', 'price'),
            ('order_date', 'order_date'),
            ('destination', 'destination'),
            ('status', 'status'),
        ),
        field_labels={
            'id': 'Order id',
            'order_date': 'Date of order',
        }
    )

    class Meta:
        model = Order
        fields = {
            'id': ['icontains'],
            'price': ['gt','lt'],
        }

class ContainerFilter(django_filters.FilterSet):
    o = django_filters.OrderingFilter(
        fields=(
            ('name', 'name'),
            ('discount', 'discount'),
            ('price', 'price'),
            ('count', 'count'),
        ),
        field_labels={
            'id': 'Order id',
            'order_date': 'Date of order',
        }
    )

    class Meta:
        model = Container
        fields = {
            'name': ['icontains'],
            'discount': ['gt','lt'],
            'price': ['gt','lt'],
            'count': ['gt','lt'],
        }
