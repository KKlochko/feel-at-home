#################################################################################
# Copyright (C) 2022 Kostiantyn Klochko <kostya_klochko@ukr.net>                #
#                                                                               #
# This file is part of feel-at-home.                                            #
#                                                                               #
# feel-at-home is free software: you can redistribute it and/or modify it under #
# the terms of the GNU Affero General Public License as published by the Free   #
# Software Foundation, either version 3 of the License, or (at your option)     #
# any later version.                                                            #
#                                                                               #
# feel-at-home is distributed in the hope that it will be useful, but WITHOUT   #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS #
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more  #
# details.                                                                      #
#                                                                               #
# You should have received a copy of the GNU Affero General Public License      #
# along with feel-at-home. If not, see <https://www.gnu.org/licenses/>.         #
#################################################################################
from django.db import models
from django.contrib.auth.models import User
from django.db.models.base import Model

class Order(models.Model):
    order_date = models.DateTimeField('date of order')
    price = models.DecimalField(max_digits=10, decimal_places=2)
    receiver = models.ForeignKey(User, on_delete=models.CASCADE)
    destination = models.CharField(max_length=256)

    # Constants for choices
    CART = "CART" # For using order as a cart
    TODO = "TODO"
    DOING = "DOING"
    CONFIRM = "CONFIRM"
    DONE = "DONE"
    CANCELED = "CANCELED"
    STATUSES = [
        (TODO, "TODO"),
        (DOING, "DOING"),
        (CONFIRM, "CONFIRM"),
        (DONE, "DONE"),
        (CANCELED, "CANCELED"),
        (CART, "CART"),
    ]

    status = models.CharField(
        max_length=8,
        choices=STATUSES,
        default=TODO,
    )

class Good(models.Model):
    name = models.CharField(max_length=64)
    description = models.TextField(max_length=5000)
    price= models.DecimalField(max_digits=10, decimal_places=2)
    count = models.PositiveIntegerField()

    def __str__(self) -> str:
        return self.name

    def is_empty(self) -> bool:
        return self.count != 0

class Container(models.Model):
    name = models.CharField(max_length=64)
    discount = models.DecimalField(max_digits=10, decimal_places=2)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    count = models.PositiveIntegerField()
    goodId = models.ForeignKey(Good, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.name

class ContainersAndOrder(models.Model):
    orderId = models.ForeignKey(Order, on_delete=models.CASCADE)
    containerId = models.ForeignKey(Container, on_delete=models.CASCADE)
