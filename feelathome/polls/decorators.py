#from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.http import Http404
from django.core.exceptions import PermissionDenied
from .models import Order, Container, ContainersAndOrder

def forbidden_order(view_func):
    """
    Return 403 error if the order owner is not the user.
    """
    def wrapper(request, order_id, *args, **kwargs):
        order = get_object_or_404(Order, pk=order_id)
        user_groups = list(map(lambda g: g.name,request.user.groups.all()))
        if not (request.user.is_staff or 'courier' in user_groups)and request.user.id != order.receiver.id:
            raise PermissionDenied
        return view_func(request, order_id, *args, **kwargs)
    return wrapper

def forbidden_cart_item(view_func):
    """
    Return 403 error if the cart item owner is not the user.
    """
    def wrapper(request, container_id, *args, **kwargs):
        user_id = request.user.id

        order_id = ContainersAndOrder.objects.select_related().filter(containerId = container_id).values('orderId').first()
        if order_id is None:
            raise Http404

        order = get_object_or_404(Order, pk=order_id['orderId'])
        if order:
            if user_id != order.receiver.id:
                raise PermissionDenied

        # Container of Order not of Cart
        if order.status != "CART":
            raise PermissionDenied

        if request.user.id != order.receiver.id:
            raise PermissionDenied
        return view_func(request, container_id, *args, **kwargs)
    return wrapper
