-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Dec 03, 2022 at 09:19 AM
-- Server version: 10.5.17-MariaDB-1:10.5.17+maria~ubu2004
-- PHP Version: 8.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maria`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_group`
--

INSERT INTO `auth_group` (`id`, `name`) VALUES
(4, 'admin'),
(5, 'courier'),
(3, 'customer'),
(2, 'shop assistant');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` bigint(20) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_group_permissions`
--

INSERT INTO `auth_group_permissions` (`id`, `group_id`, `permission_id`) VALUES
(2, 2, 5),
(3, 2, 6),
(4, 2, 7),
(1, 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add container', 1, 'add_container'),
(2, 'Can change container', 1, 'change_container'),
(3, 'Can delete container', 1, 'delete_container'),
(4, 'Can view container', 1, 'view_container'),
(5, 'Can add good', 2, 'add_good'),
(6, 'Can change good', 2, 'change_good'),
(7, 'Can delete good', 2, 'delete_good'),
(8, 'Can view good', 2, 'view_good'),
(9, 'Can add order', 3, 'add_order'),
(10, 'Can change order', 3, 'change_order'),
(11, 'Can delete order', 3, 'delete_order'),
(12, 'Can view order', 3, 'view_order'),
(13, 'Can add containers and order', 4, 'add_containersandorder'),
(14, 'Can change containers and order', 4, 'change_containersandorder'),
(15, 'Can delete containers and order', 4, 'delete_containersandorder'),
(16, 'Can view containers and order', 4, 'view_containersandorder'),
(17, 'Can add log entry', 5, 'add_logentry'),
(18, 'Can change log entry', 5, 'change_logentry'),
(19, 'Can delete log entry', 5, 'delete_logentry'),
(20, 'Can view log entry', 5, 'view_logentry'),
(21, 'Can add permission', 6, 'add_permission'),
(22, 'Can change permission', 6, 'change_permission'),
(23, 'Can delete permission', 6, 'delete_permission'),
(24, 'Can view permission', 6, 'view_permission'),
(25, 'Can add group', 7, 'add_group'),
(26, 'Can change group', 7, 'change_group'),
(27, 'Can delete group', 7, 'delete_group'),
(28, 'Can view group', 7, 'view_group'),
(29, 'Can add user', 8, 'add_user'),
(30, 'Can change user', 8, 'change_user'),
(31, 'Can delete user', 8, 'delete_user'),
(32, 'Can view user', 8, 'view_user'),
(33, 'Can add content type', 9, 'add_contenttype'),
(34, 'Can change content type', 9, 'change_contenttype'),
(35, 'Can delete content type', 9, 'delete_contenttype'),
(36, 'Can view content type', 9, 'view_contenttype'),
(37, 'Can add session', 10, 'add_session'),
(38, 'Can change session', 10, 'change_session'),
(39, 'Can delete session', 10, 'delete_session'),
(40, 'Can view session', 10, 'view_session');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$390000$LdOAeJcRMXg5rP7ESL7bit$xgrTc2wmY5Fn+Men0cF3oQA/32PcJcm8pLBKfV8oupo=', '2022-12-02 16:13:13.402701', 1, 'admin', '', '', 'admin@test.com', 1, 1, '2022-10-21 07:20:28.000000'),
(2, 'pbkdf2_sha256$390000$Dfj8Ytmn1WisMOuy3GcV2v$d5cAFo1tjZ9BFUDCh8hk5MO9E4ovisIdYNC0AMqsrD0=', '2022-11-30 17:39:00.000000', 0, 'Oleg', 'Oleg', 'Olegus', 'oleg@test.com', 0, 1, '2022-11-02 20:10:15.000000'),
(3, 'pbkdf2_sha256$390000$mvTafrYskmzH45ibKiu3zx$x704XMW4pSRjIrHbp0jB2+AK28RonosXWJX6MVp5aM4=', '2022-12-02 09:59:14.000000', 0, 'CookieSlayer', 'Cookie', 'Slayer', 'test@test.com', 0, 1, '2022-11-02 20:19:34.000000'),
(5, 'pbkdf2_sha256$390000$UaIe9yp5nd0DyLboz97Hq8$4ZndqLWqcfIF7BTfz3WJG/yoiivrB1Ve3hVvflx+9jw=', '2022-12-02 09:58:38.000000', 0, 'Kostya', 'Kostya', 'Beresa', 'test2@test.com', 1, 1, '2022-11-29 12:57:24.000000'),
(6, 'pbkdf2_sha256$390000$DpBShsIr0EwbtsHzBvTps1$TBgr8XZ6uDseohs7vmF9xeYDKqXnPRvZVuSNqjtWCfw=', '2022-12-02 09:58:59.000000', 0, 'Deliverman', 'Jo', 'Jo', 'jojo@test.com', 0, 1, '2022-11-30 14:48:31.000000');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_user_groups`
--

INSERT INTO `auth_user_groups` (`id`, `user_id`, `group_id`) VALUES
(5, 1, 3),
(1, 1, 4),
(8, 2, 3),
(7, 3, 3),
(3, 5, 2),
(2, 5, 3),
(4, 6, 3),
(6, 6, 5);

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(5, 'admin', 'logentry'),
(7, 'auth', 'group'),
(6, 'auth', 'permission'),
(8, 'auth', 'user'),
(9, 'contenttypes', 'contenttype'),
(1, 'polls', 'container'),
(4, 'polls', 'containersandorder'),
(2, 'polls', 'good'),
(3, 'polls', 'order'),
(10, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` bigint(20) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2022-10-21 07:13:27.822160'),
(2, 'auth', '0001_initial', '2022-10-21 07:13:37.700157'),
(3, 'admin', '0001_initial', '2022-10-21 07:13:40.434044'),
(4, 'admin', '0002_logentry_remove_auto_add', '2022-10-21 07:13:40.459048'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2022-10-21 07:13:40.487933'),
(6, 'contenttypes', '0002_remove_content_type_name', '2022-10-21 07:13:41.856001'),
(7, 'auth', '0002_alter_permission_name_max_length', '2022-10-21 07:13:42.677133'),
(8, 'auth', '0003_alter_user_email_max_length', '2022-10-21 07:13:42.768378'),
(9, 'auth', '0004_alter_user_username_opts', '2022-10-21 07:13:42.792796'),
(10, 'auth', '0005_alter_user_last_login_null', '2022-10-21 07:13:43.996558'),
(11, 'auth', '0006_require_contenttypes_0002', '2022-10-21 07:13:44.012776'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2022-10-21 07:13:44.038491'),
(13, 'auth', '0008_alter_user_username_max_length', '2022-10-21 07:13:44.133343'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2022-10-21 07:13:44.223550'),
(15, 'auth', '0010_alter_group_name_max_length', '2022-10-21 07:13:44.315100'),
(16, 'auth', '0011_update_proxy_permissions', '2022-10-21 07:13:44.345990'),
(17, 'auth', '0012_alter_user_first_name_max_length', '2022-10-21 07:13:44.452239'),
(18, 'polls', '0001_initial', '2022-10-21 07:13:51.172839'),
(19, 'sessions', '0001_initial', '2022-10-21 07:13:52.576148'),
(20, 'polls', '0002_auto_20221021_0728', '2022-10-21 07:29:02.073932'),
(21, 'polls', '0003_alter_order_status', '2022-11-20 20:35:15.681154');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('04ju8k75tpo3uiu5mk828vzsisp9a9ud', '.eJxVjMsOwiAQRf-FtSEw5enSvd9ABgakaiAp7cr479qkC93ec859sYDbWsM28hJmYmcm2el3i5geue2A7thunafe1mWOfFf4QQe_dsrPy-H-HVQc9VuDT1HjRAAkp2giTE4XhcIQOsgkQLgiMxIkbZVXShhvYtKqZLCWtGLvD-AHN5c:1olmMM:AIIJ29PoouKWzbxpcuxWVyYIqE7VfqszjYirEhWZ1DA', '2022-11-04 07:22:26.737264'),
('wy0vjoj33r6ya1wylwnperto7sj5mb4c', '.eJxVjMsOwiAQRf-FtSHAVB4u3fcbGpgZpGogKe3K-O_apAvd3nPOfYkpbmuZts7LNJO4CC1Ov1uK-OC6A7rHemsSW12XOcldkQftcmzEz-vh_h2U2Mu3NtpYzwEwBO0RMhvGgEppa4JyQ_aUgBgAmHyKnCg4jQxnS0Amu0G8P9iZOBA:1p0RBe:uhkXIQFK0sEzyFBIUxRQuYR4yTbqWtiC2D3DSh3ZLyU', '2022-12-14 17:47:58.038207');

-- --------------------------------------------------------

--
-- Table structure for table `polls_container`
--

CREATE TABLE `polls_container` (
  `id` bigint(20) NOT NULL,
  `name` varchar(64) NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `count` int(10) UNSIGNED NOT NULL CHECK (`count` >= 0),
  `goodId_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `polls_container`
--

INSERT INTO `polls_container` (`id`, `name`, `discount`, `price`, `count`, `goodId_id`) VALUES
(2, 'Chair', '0.00', '1000.00', 2, 3),
(3, 'Wooden Chair', '50.00', '999.99', 1, 4),
(4, 'Black table', '100.00', '2000.00', 1, 5),
(5, 'CouchDB', '0.00', '3000.00', 1, 6),
(6, 'The work desk', '0.00', '2500.00', 1, 7),
(7, 'The computer desk', '0.00', '2700.00', 1, 8),
(8, 'The gaming chair', '1000.00', '3999.00', 10, 9),
(9, 'The gaming couch', '2000.00', '6999.00', 1, 10),
(10, 'The gaming lamp', '0.00', '999.00', 2, 11),
(11, 'The gaming shelf', '0.00', '2999.00', 3, 12),
(29, 'The gaming chair', '0.00', '3999.00', 14, 9),
(30, 'Wooden Chair', '0.00', '999.99', 80, 4),
(31, 'Chair', '0.00', '1000.00', 20, 3),
(32, 'Chair', '0.00', '1000.00', 1, 3),
(33, 'Chair', '0.00', '1000.00', 2, 3),
(34, 'The gaming chair', '0.00', '3999.00', 45, 9),
(35, 'The computer desk', '0.00', '2700.00', 56, 8),
(36, 'Black table', '0.00', '2000.00', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `polls_containersandorder`
--

CREATE TABLE `polls_containersandorder` (
  `id` bigint(20) NOT NULL,
  `containerId_id` bigint(20) NOT NULL,
  `orderId_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `polls_containersandorder`
--

INSERT INTO `polls_containersandorder` (`id`, `containerId_id`, `orderId_id`) VALUES
(1, 5, 2),
(2, 2, 3),
(3, 8, 4),
(4, 11, 5),
(5, 4, 6),
(6, 10, 7),
(7, 9, 8),
(8, 7, 9),
(9, 6, 10),
(10, 3, 11),
(23, 29, 12),
(24, 30, 12),
(25, 31, 12),
(26, 32, 14),
(27, 33, 15),
(28, 34, 17),
(29, 35, 17),
(30, 36, 16);

-- --------------------------------------------------------

--
-- Table structure for table `polls_good`
--

CREATE TABLE `polls_good` (
  `id` bigint(20) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` longtext NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `count` int(10) UNSIGNED NOT NULL CHECK (`count` >= 0)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `polls_good`
--

INSERT INTO `polls_good` (`id`, `name`, `description`, `price`, `count`) VALUES
(3, 'Chair', 'The well-made chair', '1000.00', 77),
(4, 'Wooden Chair', 'The well-made wooden chair.', '999.99', 0),
(5, 'Black table', 'The well-made black table.', '2000.00', 59),
(6, 'CouchDB', 'The well-made couchDB.', '3000.00', 70),
(7, 'The work desk', 'The work desk has lots of space for your workflow.', '2500.00', 40),
(8, 'The computer desk', 'The computer desk has lots of space for a computer case, a monitor or a laptop.', '2700.00', 9),
(9, 'The gaming chair', 'The coolest comfortable chair for your play time or work.', '3999.00', 9),
(10, 'The gaming couch', 'The coolest comfortable gaming couch for gamers.', '6999.00', 46),
(11, 'The gaming lamp', 'The coolest lamp can be changed the color of the light. \r\n\r\nCan shine as rainbow gradient.', '999.00', 35),
(12, 'The gaming shelf', 'The coolest shelf that can store all your collection of games.\r\n\r\nCan hold 100 kg of items.', '2999.00', 75);

-- --------------------------------------------------------

--
-- Table structure for table `polls_order`
--

CREATE TABLE `polls_order` (
  `id` bigint(20) NOT NULL,
  `order_date` datetime(6) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `destination` varchar(256) NOT NULL,
  `status` varchar(8) NOT NULL,
  `receiver_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `polls_order`
--

INSERT INTO `polls_order` (`id`, `order_date`, `price`, `destination`, `status`, `receiver_id`) VALUES
(2, '2022-11-02 19:10:00.000000', '3000.00', 'Admin\'s office', 'DOING', 1),
(3, '2022-11-04 08:20:00.000000', '2000.00', 'Admin\'s office', 'DOING', 1),
(4, '2022-10-25 12:34:00.000000', '38990.00', 'Admin\'s office', 'DONE', 1),
(5, '2022-12-05 19:10:00.000000', '8997.00', 'Admin\'s office', 'DONE', 1),
(6, '2022-11-05 19:10:00.000000', '1900.00', 'Cookie\'s home', 'DOING', 3),
(7, '2022-10-07 12:23:00.000000', '1998.00', 'Cookie\'s home', 'DONE', 3),
(8, '2022-12-25 08:30:00.000000', '4999.00', 'Cookie\'s home', 'TODO', 3),
(9, '2022-11-02 19:10:00.000000', '2700.00', 'Oleg\'s home', 'DOING', 2),
(10, '2022-10-03 11:30:00.000000', '2500.00', 'Oleg\'s home', 'DONE', 2),
(11, '2022-12-15 09:40:00.000000', '949.99', 'Oleg\'s home', 'TODO', 2),
(12, '2022-11-20 19:34:22.223499', '5998.99', 'home', 'TODO', 1),
(13, '2022-11-29 13:26:06.546168', '0.00', '', 'CART', 5),
(14, '2022-12-01 15:43:17.346047', '1000.00', 'Home', 'TODO', 1),
(15, '2022-11-30 17:39:05.778496', '0.00', '', 'CART', 2),
(16, '2022-12-01 15:43:21.292982', '2000.00', '', 'CART', 1),
(17, '2022-12-02 09:57:26.319288', '6699.00', 'Home', 'CONFIRM', 3),
(18, '2022-12-02 09:59:25.127931', '0.00', '', 'CART', 3);

-- --------------------------------------------------------

--
-- Table structure for table `Test`
--

CREATE TABLE `Test` (
  `id` int(11) NOT NULL,
  `num` int(11) DEFAULT NULL,
  `string` char(128) DEFAULT NULL,
  `text` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `polls_container`
--
ALTER TABLE `polls_container`
  ADD PRIMARY KEY (`id`),
  ADD KEY `polls_container_goodId_id_421c01f0_fk_polls_good_id` (`goodId_id`);

--
-- Indexes for table `polls_containersandorder`
--
ALTER TABLE `polls_containersandorder`
  ADD PRIMARY KEY (`id`),
  ADD KEY `polls_containersando_containerId_id_11a40c07_fk_polls_con` (`containerId_id`),
  ADD KEY `polls_containersandorder_orderId_id_2c477b5a_fk_polls_order_id` (`orderId_id`);

--
-- Indexes for table `polls_good`
--
ALTER TABLE `polls_good`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `polls_order`
--
ALTER TABLE `polls_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `polls_order_receiver_id_2d382b2c_fk_auth_user_id` (`receiver_id`);

--
-- Indexes for table `Test`
--
ALTER TABLE `Test`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `polls_container`
--
ALTER TABLE `polls_container`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `polls_containersandorder`
--
ALTER TABLE `polls_containersandorder`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `polls_good`
--
ALTER TABLE `polls_good`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `polls_order`
--
ALTER TABLE `polls_order`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `polls_container`
--
ALTER TABLE `polls_container`
  ADD CONSTRAINT `polls_container_goodId_id_421c01f0_fk_polls_good_id` FOREIGN KEY (`goodId_id`) REFERENCES `polls_good` (`id`);

--
-- Constraints for table `polls_containersandorder`
--
ALTER TABLE `polls_containersandorder`
  ADD CONSTRAINT `polls_containersando_containerId_id_11a40c07_fk_polls_con` FOREIGN KEY (`containerId_id`) REFERENCES `polls_container` (`id`),
  ADD CONSTRAINT `polls_containersandorder_orderId_id_2c477b5a_fk_polls_order_id` FOREIGN KEY (`orderId_id`) REFERENCES `polls_order` (`id`);

--
-- Constraints for table `polls_order`
--
ALTER TABLE `polls_order`
  ADD CONSTRAINT `polls_order_receiver_id_2d382b2c_fk_auth_user_id` FOREIGN KEY (`receiver_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
